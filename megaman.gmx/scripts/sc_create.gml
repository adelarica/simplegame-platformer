grav = 0.4;
hsp = 0;
hsp_moving_platform = 0;
vsp = 0;
movespeed = 4;

jumpspeed_normal =7;

jumpspeed = jumpspeed_normal;

//IA Checks
hcollision = false;
fearofheights = false;

//Constants
grounded = false;
jumping = false;


//Horizontal key pressed count
hkp_count = 0;
hkp_count_small = 2;
hkp_count_big = 5;

//Init variables
key_left = 0;
key_right = 0;
key_jump = false;
