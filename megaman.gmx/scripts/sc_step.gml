move = key_left + key_right;
hsp = move*movespeed;

if(vsp<10)vsp+=grav;


if(place_meeting(x,y+1,obj_wall) || place_meeting(x,y+1,obj_floor_blue) ||place_meeting(x,y+1,obj_floor_orange) ||place_meeting(x,y+1,obj_moving_wall))
{
    //Check if recently grounded
    if(!grounded && !key_jump){
        hkp_count = 0; //Init horizontal count
        jumping = false;
    }else if(grounded && key_jump){ //recently jumping
        jumping = true;
    }

    //Check if player grounded
    grounded = !key_jump;
    
    vsp = key_jump * -jumpspeed;
}
if(key_jump && ((place_meeting(x+1,y,obj_wall))|| (place_meeting(x-1,y,obj_wall)))) { // Saltar en paredes
    vsp = -jumpspeed;
}


//Init hsp_jump_applied
if(grounded){
    hsp_jump_applied = 0;
}

//Check horizontal counts
if(move!=0 && grounded){
 hkp_count++;
}else if(move==0 && grounded){
 hkp_count=0;
}

var hsp_final = hsp + hsp_moving_platform;
hsp_moving_platform = 0;

//horizontal collision
if(place_meeting(x+hsp_final,y,obj_wall)|| place_meeting(x+hsp_final,y,obj_door) || place_meeting(x+hsp_final,y,obj_moving_wall))
{
    while(!place_meeting(x+sign(hsp_final),y,obj_wall) && !place_meeting(x+sign(hsp_final),y,obj_door) && !place_meeting(x+sign(hsp_final),y,obj_moving_wall))
    {
        x+= sign(hsp);
    }
    hsp_final = 0;
    hsp = 0;
    hcollision = true;
}

x+= hsp_final;
//vertical collision
if(place_meeting(x,y+vsp,obj_wall) || place_meeting(x,y+vsp,obj_floor_blue) || place_meeting(x,y+vsp,obj_floor_orange) || place_meeting(x,y+vsp,obj_moving_wall))
{
    while(!place_meeting(x, sign(vsp) + y,obj_wall) && !place_meeting(x,sign(vsp)+y,obj_floor_blue) && !place_meeting(x,sign(vsp)+y,obj_floor_orange) && !place_meeting(x,sign(vsp)+y,obj_moving_wall))
    {
        y+= sign(vsp);
    }
    vsp = 0;
}

y+= vsp;
//ground collision
if(fearofheights && (place_meeting(x,y+8,obj_wall) )&& 
    !position_meeting(x+(sprite_width/2)*dir,y+(sprite_height/2)+8,obj_wall)){
    dir *= -1;
}

